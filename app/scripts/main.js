// requestAnimationFrame polyfill
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());
// end polyfill

/*
	General
*/
var $w = $(window),
	w_width = $w.width(),
	w_height = $w.height(),
	is_mobile = function() {
		return w_width < 576 || w_height < 576? true : false;
	};

$w.resize(function() {
	w_width = $w.width();
	w_height = $w.height();
});

var hidePopup = function($popup) {
		var hide_c = 'popup-menu--hide',
			showing_c = 'popup-menu--showing';

		$popup.addClass(hide_c);

		setTimeout(function() {
			$popup.removeClass(showing_c + ' ' + hide_c);
		}, 400);
	},
	showPopup = function($popup) {
		var $popup_menu = $popup.find('.popup-menu'),
			show_c = 'popup-menu--show',
			showing_c = 'popup-menu--showing';

		$popup.addClass(show_c);

		setTimeout(function() {
			$popup.addClass(showing_c).removeClass(show_c);
		}, 400);

		$popup.click(function() {
			hidePopup($popup);
		});

		$popup_menu.click(function(e) {
			e.stopPropagation();
		});
	};

function resetSession () {
	if(sessionStorage.getItem('level')) {
		sessionStorage.setItem('level', 0);
	}
}

function initDate() {
	var $datetime_picker = $('.datetime-picker'),
		is_popup = is_mobile();

	$datetime_picker.DateTimePicker({
		isPopup: is_popup,
		animationDuration: 0,
		addEventHandlers: function() {
			var datetime_picker = this;

			$w.resize(function() {
				datetime_picker.setIsPopup(is_mobile());
			});
		}
	});
}

function toggleSubmit(proceed, $btn) {
	if(proceed) {
		$btn.removeClass('disabled').prop('disabled', false);
	} else {
		$btn.addClass('disabled').prop('disabled', true);
	}
}

function showError($anchor, message, target) {
	var $next_el = $anchor.next(),
		error_c = 'form-control-error';

	if($next_el.hasClass(error_c)) {
		$next_el.remove();
	}

	var error_html = '<label class="form-control-error" for="'+ target +'">'+ message +'</label>';

	$anchor.after(error_html);
}

/*
	Login
*/
function initLogin() {
	var $email = $('#login_email'),
		$password = $('#login_password'),
		$login_input = $('#login_email, #login_password'),
		$login_submit = $('#login_submit'),
		email,
		password,
		proceed;

	resetSession();

	var verifyEmail = function(email) {
			return /(.+)@(.+){2,}\.(.+){2,}/.test(email);
		},
		verifyLogin = function(e) {
			proceed = false;

			if($password.val() !== '' && $email.val() !== '' && verifyEmail($email.val())) {
				proceed = true;
				email = $email.val();
				password = $password.val();
			}

			if(e.keyCode == 13 && proceed) {
				$login_submit.click();
			}

			toggleSubmit(proceed, $login_submit);
		};

	$login_input.on('change input paste keydown', verifyLogin);

	$login_submit.click(function(e) {
		e.preventDefault();
		var $btn = $(this);

		if(proceed) {

			var url = '/action/login_action.php',
				data = {'login_email': email, 'login_password': password};

			$.ajax({
				type: 'POST',
				url: url,
				data: data
			}).done(function(data) {
				var result = data.split('^')[1];

				result = result? JSON.parse(result) : false;

				if(result.status) {
					window.location.replace('/receipt');
				} else {
					showError($btn, result.message, $email.prop('id'));
				}
			});
		}
	});
}

var is_home = $('.home').length? true : false;

$(function() {
	if(is_home) {
		initHome();
	}
});

function initHome() {
	var $home_rules_cta = $('#home_rules_cta'),
		$home_rules = $('#home_rules'),
		home_rules_pos = $home_rules.position().top,
		$home_prizes_cta = $('#home_prizes_cta'),
		$home_prizes = $('#home_prizes'),
		home_prizes_pos = $home_prizes.position().top,
		scroll_to = function(pos) {
			$('html, body').animate({
				scrollTop: pos
			}, 1000);
		};

	resetSession();

	$home_rules_cta.click(function(e) {
		scroll_to(home_rules_pos);

		e.preventDefault();
	});

	$home_prizes_cta.click(function(e) {
		scroll_to(home_prizes_pos);

		e.preventDefault();
	});
}

var is_login = $('.login').length? true : false;

$(function() {
	if(is_login) {
		initLogin();
	}
});

/*
	Receipt
*/
function initReceipt() {
	// intro
	var $receipt_selection = $('#receipt_selection'),
		$receipt_selection_btn = $receipt_selection.find('.btn'),
		receipt_intro = true,

	// form
		current_selection = 'counter',
		$counter_form = $('#counter_form'),
		$online_form = $('#online_form'),
		$receipt_toggle = $('#receipt_selection_toggle'),
		$receipt_input = $('#receipt_input'),
		$receipt_submit = $('#submit_receipt'),
		receipt_number,

	// sample
		$sample_link = $('.receipt-sample-link'),
		$sample_popup = $('#sample_popup'),
		$counter_sample = $('#counter_sample'),
		$online_sample = $('#online_sample'),
		$sample_close = $('#sample_popup_btn'),

	// verification
		$receipt_form_inputs = $receipt_input.find('.form-control'),
		$counter_number = $('#receipt_number_1'),
		$online_number = $('#receipt_number_2'),
		$online_date = $('#receipt_date'),
		proceed;

	resetSession();

	var verifyNumber = function(v) {
			var valid = false,
				length_required = current_selection === 'counter'? 14 : 5;

			v = v.trim();

			if(/^\d+$/.test(v) && v.length >= length_required) {
				valid = true;
			}

			return valid;
		},
		verifyDate = function(v) {
			var valid = false,
				date_arr = v.split('-');

			if(date_arr.length !== 3) {
				return valid;
			}

			valid = true;

			$.each(date_arr, function() {
				if(!/^\d+$/.test(this)) {
					valid = false;
				}
			});

			return valid;
		},
		verifyReceipt = function(e) {
			proceed = false;

			if(current_selection === 'counter') {
				if(verifyNumber($counter_number.val())) {
					proceed = true;
				}
			} else {
				if(verifyNumber($online_number.val()) && verifyDate($online_date.val())) {
					proceed = true;
				}
			}

			if(e && e.keyCode == 13 && proceed) {
				$receipt_submit.click();
			}

			toggleSubmit(proceed, $receipt_submit);
		};

	$receipt_form_inputs.on('change input paste keydown', verifyReceipt);

	var process_selection = function() {
		var selection_label,
			$show,
			$hide;

		if(current_selection === 'counter') {
			$show = $counter_form;
			$hide = $online_form;
			selection_label = 'GSC COUNTER';
		} else {
			$show = $online_form;
			$hide = $counter_form;
			selection_label = 'GSC ONLINE';
		}

		if(receipt_intro) {
			$show.removeClass('hide');
		} else {
			var hide_c = 'receipt-input--toggle-hide',
				show_c = 'receipt-input--toggle-show',
				hiding_c = 'receipt-input--toggling',
				current_height = $receipt_input.height();

			$receipt_input.height(current_height).addClass(hide_c);

			setTimeout(function() {
				$hide.addClass('hide');
				$receipt_input.addClass(hiding_c);
				verifyReceipt();

				setTimeout(function() {
					$show.removeClass('hide');
					$receipt_input.addClass(show_c).removeClass(hide_c);

					setTimeout(function() {
						$receipt_input.removeClass(show_c + ' ' + hiding_c).css('height', '');
					}, 300);
				}, 100);
			}, 300);
		}

		$receipt_toggle.html(selection_label);
	}

	$receipt_selection_btn.click(function(e) {
		e.preventDefault();

		var $btn = $(this),
			selection = $btn.val();

		current_selection = selection;

		process_selection();

		receipt_intro = false;

		$receipt_toggle.html();

		var hide_c = 'receipt-section--hide',
			show_c = 'receipt-section--show';

		$receipt_selection.addClass(hide_c);

		setTimeout(function() {
			$receipt_selection.addClass('hide').removeClass(hide_c);
			$receipt_input.addClass(show_c);
			setTimeout(function() {
				$receipt_input.removeClass(show_c + ' hide');
			}, 500);
		}, 300);
	});

	$receipt_toggle.click(function(e) {
		e.preventDefault();

		current_selection = current_selection  === 'counter'? 'online' : 'counter';

		process_selection();
	});

	$sample_link.click(function(e) {
		e.preventDefault();

		var $hide,
			$show;

		if(current_selection === 'counter') {
			$hide = $online_sample;
			$show = $counter_sample;
		} else {
			$hide = $counter_sample;
			$show = $online_sample;
		}

		$hide.addClass('hide');
		$show.removeClass('hide');

		showPopup($sample_popup);
	});

	$sample_close.click(function(e) {
		e.preventDefault();

		hidePopup($sample_popup);
	});

	$sample_popup.click(function() {
		hidePopup($sample_popup);
	});

	$receipt_submit.click(function(e) {
		e.preventDefault();

		var $btn = $(this);

		if(proceed) {
			var url = '../action/receipt_action.php',
				receipt_id,
				data;

			if(current_selection === 'online') {
				receipt_id = $online_date.val().split('-').join('') + $online_number.val();
			} else {
				receipt_id = $counter_number.val();
			}

			data = {'receipt_type': current_selection, 'receipt_id': receipt_id};

			$.ajax({
				type: 'POST',
				url: url,
				data: data
			}).done(function(data) {
				var result = data.split('^')[1];

				result = result? JSON.parse(result) : false;

				if(result.status) {
					window.location.replace('/game');
				} else {
					var $target = current_selection === 'counter'? $counter_number : $online_number,
						target_id = $target.prop('id');

					showError($btn, result.message, target_id);
				}
			});
		}
	});

	initDate();
}
var is_receipt = $('.receipt').length? true : false;

$(function() {
	if(is_receipt) {
		initReceipt();
	}
});

/*
	Game
*/
function initImageScroll() {
	var $game_container = $('#game_container'),
		$game_view = $('#game_view'),
		game_view = $game_view[0],
		$game_image = $('#game_image'),
		game_image = $game_image[0],
		image_width = game_view.clientWidth,
		image_start_x = 0,
		max_scroll_dist,
		scrollable = false,
		drag_start_x = 0,
		drag_end_x = 0,
		dragging = false;

	var show_move_c = 'show-move-indicator',
		hide_move_c = 'hide-move-indicator';

	var setViewSize = function() {
			var image_rect = game_image.getBoundingClientRect(),
				image_w = image_rect.width,
				image_h = image_rect.height;

			$game_view.width(image_w).height(image_h);
		},
		getX = function(e) {
			return e.touches && e.touches.length? e.touches[0].clientX : e.clientX;
		},
		initScrollValue = function() {
			image_width = game_image.clientWidth;
			max_scroll_dist = image_width - w_width;
			scrollable = max_scroll_dist > 0? true : false;

			if(scrollable) {
				$game_container.addClass(show_move_c).removeClass(hide_move_c);
			} else {
				$game_container.removeClass(show_move_c);
			}
		},
		updateImagePos = function(e) {
			drag_end_x = getX(e);
		},
		moveImage = function() {
			if(!dragging) {
				return;
			}

			var scroll_x = drag_start_x - drag_end_x - image_start_x;

			scroll_x = scroll_x > max_scroll_dist? max_scroll_dist : scroll_x < 0? 0 : scroll_x;

			scroll_x = 0 - scroll_x;

			game_view.style.transform = 'translateX('+ scroll_x +'px)';

			requestAnimationFrame(moveImage);
		},
		onDragMove = function(e) {
			if(!dragging) {
				return;
			}

			updateImagePos(e);
			requestAnimationFrame(moveImage);

			e.preventDefault();
		},
		onDragEnd = function(e) {
			dragging = false;

			$game_container.unbind('mousemove touchmove mouseup touchend');

			e.preventDefault();
		},
		onDragStart = function(e) {
			var draggable = true,
				non_el = ['.game-answer', '.game-pin'];

			for(var c in non_el) {
				if($(e.target).closest(non_el[c]).length) {
					draggable = false;
				}
			}

			if(scrollable && draggable) {
				$(document).trigger('dragview');

				dragging = true;
				drag_start_x = getX(e);
				image_start_x = game_image.getBoundingClientRect().left;

				$game_container.addClass(hide_move_c);

				$game_container.on('mousemove touchmove', onDragMove).on('mouseup touchend', onDragEnd);

				e.preventDefault();
			}
		},
		initView = function() {
			setViewSize();
			initScrollValue();
		};

	$game_container.on('mousedown touchstart', onDragStart);

	initView();

	$w.resize(function() {
		game_view.style.width = '';
		game_view.style.height = '';
		game_view.style.transform = '';
		initView();
	});
}

function initGame() {
	var $game_container = $('#game_container'),

		// instruction popup / game intro
		$popup = $('#game_instructions'),
		$popup_content = $('#instruction_content'),
		$popup_content_block = $popup_content.children(),
		$popup_levels = $popup.find('.game-popup-levels').children(),
		$popup_close = $('#close_instruction'),
		intro_c = 'game-intro',
		$help = $('#game_help_btn'),

		// main game view
		$view = $('#game_view'),
		show_view_c = 'game-view--show',
		$image = $('#game_image'),
		// total of 3 levels: 0, 1, 2
		level = 0,
		total_level = 3,
		// current in game score
		score = 0,
		// score needed to win for each level
		level_max_score = [10, 15, 5],
		// stage of the level, 0 = intro, 1 = in progress, 2 = win
		stage = 0,

		// game header ui
		$game_level = $('#game_level'),
		$game_progress = $('#game_progress'),
		$current_score = $('#current_score'),
		$max_score = $('#max_score'),

		// question trigger blocks
		$question = $('#question_container'),
		questions,
		$question_blocks,

		// to track the question being answered
		current_question,

		// answer list
		answers,
		$answer_list,
		// the key for answer and question
		answer_key,

		// answer pin
		pins,
		$pin_container,

		// timer
		$timer,
		timer,
		total_time = 30000,
		time_start,
		time_end,
		time_left,
		lose = false,

		// win screen
		$game_win = $('#game_win_screen'),
		$close_win = $('#game_win_btn'),

		// retry buttons
		$win_retry = $('#end_retry'),
		$lose_retry = $('#lose_retry'),

		// facebook share
		$facebook = $('#end_facebook'),
		fb_app_id = 1929999833945417,
		fb_link = 'http://popcornhunt.gsc.com.my',
		fb_quote = '',

		// game sections: 0 = play, 1 = lose, 2 = end
		$game_sec = [$('#game_play'), $('#game_lose'), $('#game_end')];

	var showIntro = function() {
			var popup_anim_duration = 400,
				active_c = 'active';

			$popup.addClass(intro_c);
			$popup_close.html('START HUNTING');

			// show intro of current level
			$popup_content_block.eq(level).addClass(active_c).siblings().removeClass(active_c);

			showPopup($popup);

			var $current_level = $popup_levels.eq(level),
				$previous_level,
				complete_c = 'complete',
				reveal_level_delay = 0,
				delay_increment = 250,
				animatePopupLevel = function(i) {
					var i = i || 0;

					setTimeout(function() {
						var anim_c = i == level? active_c : complete_c + ' ' + active_c,
							$level = $popup_levels.eq(i);

						$level.addClass(anim_c);

						if(i < level) {
							i++;
							reveal_level_delay += delay_increment;

							animatePopupLevel(i);
						}
					}, reveal_level_delay);
				};

			$popup_levels.removeClass(complete_c + ' ' + active_c);

			setTimeout(function() {
				animatePopupLevel();
			}, popup_anim_duration);
		},
		afterIntro = function() {
			setTimeout(function() {
				if(stage === 0) {
					$popup.removeClass(intro_c);
					$popup_close.html('CONTINUE');

					stage = 1;
				}

				if(level === 2) {
					startTimer();
				}
			}, 400);
		},
		startTimer = function() {
			if(timer) {
				time_start = new Date();

				timer.play();
			}
		},
		formatTime = function(time) {

			var pad = function(number, length) {
					var str = '' + number;
					while (str.length < length) {str = '0' + str;}
					return str;
				};

			time = time / 10;

			var sec = parseInt(time / 100),
				hundredths = parseInt(time - (sec * 100));

			return pad(sec, 2) + '.' + pad(hundredths, 2);
		},
		initTimer = function() {
			var	time_step = 120,
				updateTimer = function() {
					$timer.html(formatTime(time_left));

					if(time_left === 0) {
						timer.stop();
						showLose();
					}

					time_left -= time_step;

					if(time_left < 0) time_left = 0;
				};

			time_left = total_time;

			$timer = $('#game_timer');

			$timer.html(formatTime(time_left));

			timer = $.timer(updateTimer, time_step, false);
		},
		hideView = function() {
			$view.removeClass(show_view_c);
		},
		showView = function() {
			$view.addClass(show_view_c);
		},
		hideWin = function() {
			var show_c = 'game-win--show',
				showing_c = 'game-win--showing',
				hide_c = 'game-win--hide';

			$game_win.addClass(hide_c);

			var clear_game = level >= total_level? true: false;

			setTimeout(function() {
				$game_win.removeClass(show_c + ' ' + showing_c + ' ' + hide_c);
				hideView();
				setTimeout(function() {
					if(clear_game) {
						showEnd();
					} else {
						initLevel();
					}
				}, 300);
			}, 500);

		},
		showWin = function() {
			var show_c = 'game-win--show',
				showing_c = 'game-win--showing';

			$game_win.addClass(show_c);

			setTimeout(function() {
				$game_win.addClass(showing_c);
			}, 1000);
		},
		showLose = function() {
			lose = true;
			showSection(1);
		},
		showEnd = function() {
			var $time_used = $('#time_used'),
				time_used = time_end - time_start,
				formatted_time = parseFloat(formatTime(time_used)).toFixed(2);

			$time_used.html(formatted_time);

			fb_quote = 'I found '+ level_max_score[2] +' Toys in '+ formatted_time +' seconds! Can you do better?';

			setLevel(0);
			showSection(2);

			// send time to backend
			var url = '/action/win_action.php',
				data = {'time_used': time_used};

			$.ajax({
				type: 'POST',
				url: url,
				data: data
			});
		},
		getRatio = function() {
			var init_h = 1000,
				image_h = $image[0].getBoundingClientRect().height,
				ratio = image_h / init_h;

			return ratio;
		},
		setLevel = function(new_level) {
			level = new_level;

			sessionStorage.setItem('level', level);
		},
		updateLevelUI = function() {
			var display_level = level + 1;
			$game_level.html(display_level);
			$max_score.html(level_max_score[level]);
		},
		updateScoreUI = function(add) {
			var addScore = function() {
					var add_c = 'game-progress--add',
						anim_duration = 800;

					if($game_progress.hasClass(add_c)) {
						setTimeout(function() {
							$game_progress.removeClass(add_c);
							addScore();
						}, anim_duration / 2);

						return;
					}

					score++;
					$current_score.html(score);
					$game_progress.addClass(add_c);

					var clear_level = score >= level_max_score[level]? true : false;

					if(clear_level) {
						if(level === 2) {
							time_end = new Date();
							timer.stop();
						}
					}

					setTimeout(function() {
						if(clear_level && !lose) {
							showWin();
							setLevel(level + 1);
						}

						$game_progress.removeClass(add_c);
					}, anim_duration);
				};

			if(add) {
				if(score < level_max_score[level]) {
					addScore();
				}
			} else {
				$current_score.html(score);
			}
		},
		hideAnswerList = function() {
			var show_c = 'game-answer--show',
				hide_c = 'game-answer--hide',
				hide_anim_duration = 300;

			if($answer_list && !$answer_list.hasClass(show_c)) {
				return;
			}

			$answer_list.addClass(hide_c);

			setTimeout(function() {
				$answer_list.removeClass(show_c + ' ' + hide_c);
			}, hide_anim_duration);

			$(document).unbind('click dragview');
		},
		setAnswerListPos = function(e) {
			var mobile = is_mobile(),
				in_view = $answer_list.hasClass(center_c)? false : true,
				center_c = 'game-answer--centered';

			if(mobile) {
				if(in_view) {
					$answer_list.addClass(center_c).appendTo($game_container);
				}
			} else {
				if(!in_view) {
					$answer_list.removeClass(center_c).appendTo($view);
				}
			}

			var answer_rect = $answer_list[0].getBoundingClientRect(),
				answer_d = {top: answer_rect.top, left: answer_rect.left, width: $answer_list.outerWidth(), height: $answer_list.outerHeight()},
				list_x = 0, list_y = 0;

			if(mobile) {
				list_x = 'calc(50% - '+ parseInt(answer_d.width / 2) +'px)';
				list_y = 'calc(50% - '+ parseInt(answer_d.height / 2) +'px)';
			} else {
				var view_rect = $view[0].getBoundingClientRect(),
					getX = function(e) {
						return e.changedTouches && e.changedTouches.length? e.changedTouches[0].clientX : e.clientX;
					},
					getY = function(e) {
						return e.changedTouches && e.changedTouches.length? e.changedTouches[0].clientY : e.clientY;
					},
					click_x = getX(e),
					click_y = getY(e),
					// true = up, false = down
					up_down = false,
					// true = left, false = right
					left_right = false;

				if(click_x + answer_d.width > view_rect.left + view_rect.width) {
					left_right = true;
				}

				if(click_y + answer_d.height > view_rect.top + view_rect.height) {
					up_down = true;
				}

				list_x = click_x - view_rect.left,
				list_y = click_y - view_rect.top;

				// margin from click
				var	click_margin = 20;

				list_x = left_right? list_x - (answer_d.width + click_margin) : list_x + click_margin;
				list_y = up_down? list_y - (answer_d.height + click_margin) : list_y + click_margin;

				list_x = list_x < 0? 0 : !left_right && list_x + answer_d.width > view_rect.width? list_x - (list_x + answer_d.width - view_rect.width) : list_x;
				list_y = list_y < 0? 0 : !up_down && list_y + answer_d.height > view_rect.height? list_y - (list_y + answer_d.height - view_rect.height) : list_y;

			}

			$answer_list.css({'top': list_y,'left': list_x});
		},
		showAnswerList = function(e) {
			var show_c = 'game-answer--show';

			if($answer_list.hasClass(show_c)) {
				return;
			}

			$answer_list.addClass(show_c);

			setAnswerListPos(e);

			e.stopPropagation();

			setTimeout(function() {
				$(document).on('click dragview', function(e) {
					if(!$answer_list[0].contains(e.target)) {
						hideAnswerList();
					}
				});
			}, 100);
		},
		bindQuestion = function() {
			var popcornAnswer = function(e) {
					var $block = $(this);

					if($block.data('correct')) {
						return;
					}

					var popcorn_c = 'game-question-popcorn',
						anim_duration = 300;

					$block.html('<div class="'+ popcorn_c +'"></div>');

					updateScoreUI(true);

					$block.data('correct', true);

					e.preventDefault();
				},
				movieAnswer = function(e, $block) {
					var $block = $block || $(this),
						block_id = $block.prop('id'),
						qid = block_id.split('_')[2];

					if($block.data('answered')) {
						return;
					}

					// set current question
					current_question = qid;

					// show answer list
					var show_answer_c = 'game-answer--show';

					// if answer list is open, close and reopen
					if($answer_list.hasClass(show_answer_c)) {
						hideAnswerList();

						setTimeout(function() {
							movieAnswer(e, $block);
						}, 300);

						return;
					}

					showAnswerList(e);

					e.preventDefault();
				};

			var questionTrigger = level > 0? movieAnswer : popcornAnswer;

			$question_blocks.on('click touchend', questionTrigger);
		},
		setBlockSizePos = function() {
			var ratio = getRatio(),
				getAdjusted = function(v) {
					return parseInt(v * ratio);
				};

			$.each($question_blocks, function(k, el) {
				var $block = $(el),
					d;

				for(d in questions[k]) {
					$block.css(d, getAdjusted(questions[k][d]));
				}
			});
		},
		initPin = function() {
			if($pin_container) {
				$pin_container.remove();
				$pin_container = undefined;
			}

			var pin_container_id = 'pin_container';

			$view.append('<div class="game-pin-container" id="'+ pin_container_id +'">');

			$pin_container = $('#' + pin_container_id);
		},
		bindAnswerList = function() {
			var $answer_btn = $answer_list.find('.game-answer-btn'),
				processMovieAnswer = function (e) {

					var $btn = $(this),
						bid = $btn.prop('id'),
						selected_answer = parseInt(bid.split('_')[2]),
						selected_answer_name = $btn.find('.game-answer-title').html(),
						correct_answer = parseInt(answer_key[current_question]),
						correct = false;

					if($btn.data('answered')) {
						return;
					}

					var answered_c = 'game-answer-btn--answered';

					$btn.addClass(answered_c).data('answered', true);

					if(selected_answer === correct_answer) {
						correct = true;
					}

					var $q_block = $('#question_'+ level +'_'+ current_question);

					$q_block.data('answered', true);

					// generate pin
					var pin_id = 'pin_'+ selected_answer +'_'+ current_question,
						pin_html = 	'<div class="game-pin" id="'+ pin_id +'">' +
										'<p class="game-pin__text">'+ selected_answer_name +'</p>' +
									'</div>';

					$pin_container.append(pin_html);

					// position pin
					var view_rect = $view[0].getBoundingClientRect(),
						q_rect = $q_block[0].getBoundingClientRect(),
						$pin = $('#' + pin_id),
						pin_rect = {width: $pin.outerWidth(), height: $pin.outerHeight()},
						pin_x = q_rect.left - view_rect.left + q_rect.width / 2 - pin_rect.width / 2,
						pin_y = q_rect.top - view_rect.top + q_rect.height / 2 - pin_rect.height,
						getCalcPos = function(p, d, c) {
							var d_half = d / 2,
								p_percent = (p + d_half) / c * 100;

							return 'calc('+ p_percent +'% - '+ d_half +'px)';
						};

					// prevent out of view
					pin_x = pin_x > 0? pin_x : correct? 0 : 10;
					pin_y = pin_y < 0? 0 : pin_y;

					pin_x = getCalcPos(pin_x, pin_rect.width, view_rect.width);
					pin_y = getCalcPos(pin_y, pin_rect.height, view_rect.height);

					$pin.css({'top': pin_y, 'left': pin_x}).hide();

					// reveal pin answer
					var pin_answer_c = correct? 'game-pin--right' : 'game-pin--wrong',
						drop_answer_delay = 200,
						reveal_answer_delay = 500;

					setTimeout(function() {
						$pin.show();

						setTimeout(function() {
							$pin.addClass(pin_answer_c);

							if(correct) {
								updateScoreUI(true);
							}
						}, reveal_answer_delay);
					}, drop_answer_delay);

					if(!correct) {
						// if wrong bind pin click
						$pin.click(function(e) {
							// remove pin and open up answer list
							var $pin = $(this),
								pid = $pin.prop('id'),
								pid_arr = pid.split('_'),
								bid = pid_arr[1],
								qid = pid_arr[2],
								remove_c = 'game-pin--remove',
								remove_anim_delay = 300;

							$pin.addClass(remove_c);

							current_question = qid;

							var $q_block =  $('#question_'+ level +'_'+ qid),
								$a_btn = $('#answer_'+ level +'_'+ bid);

							$q_block.data('answered', false);
							$a_btn.data('answered', false).removeClass(answered_c);

							setTimeout(function() {
								$pin.remove();
							}, remove_anim_delay);

							showAnswerList(e);
						});
					}

					hideAnswerList();

					e.preventDefault();
				};

			$answer_btn.click(processMovieAnswer);
		},
		generateAnswerList = function() {
			if($answer_list) {
				$answer_list.remove();
				$answer_list = undefined;
			}

			var answers_arr = [];

			for(var k in answers) {
				answers_arr.push(answers[k]);
				answers_arr[k]['qid'] = k;
			}

			// sort alphabetically by object name
			var sortByName = function(a, b) {
					var name_a = a.name.toLowerCase(),
						name_b = b.name.toLowerCase();

					return name_a > name_b? 1 : name_a < name_b? -1 : 0;
				};

			answers_arr.sort(sortByName);

			answer_key = [];

			$.each(answers_arr, function(k, sorted_answer) {
				answer_key[sorted_answer.qid] = k;
			});

			var answer_list_html = '',
				answer_list_id = 'answer_list';

			answer_list_html = 	'<div class="game-answer" id="'+ answer_list_id +'">' +
									'<ul class="game-answer-list">';

			$.each(answers_arr, function(k, answer) {
				answer_list_html += 	'<li>' +
											'<button class="game-answer-btn" id="answer_'+ level +'_'+ k +'">' +
												'<img src="'+ answer.thumb +'" class="game-answer-image" alt="'+ answer.name +'">' +
												'<p class="game-answer-title">'+ answer.name +'</p>' +
											'</button>' +
										'</li>';
			});

			answer_list_html += 	'</ul>' +
								'</div>';

			$view.append(answer_list_html);

			$answer_list = $('#' + answer_list_id);

			bindAnswerList();
		},
		generateQuestion = function() {
			$question.html('');

			var question_c = 'game-question';

			for(var k in questions) {
				$question.append('<div class="'+ question_c +'" id="question_'+ level +'_'+ k +'">');
			}

			$question_blocks = $question.children('.' + question_c);

			setBlockSizePos();
			bindQuestion();
			showView();

			var view_anim_duration = 300;

			setTimeout(function() {
				showIntro();
			}, view_anim_duration);
		},
		getQuestion = function() {
			var question_url = '/data/level_'+ level +'.json';

			$.ajax({
				url: question_url,
				isLocal: true
			}).done(function(data) {
				if(level === 0) {
					questions = data;
				} else {

					questions = data.questions;
					answers = data.answers;
					generateAnswerList();
					initPin();
				}

				generateQuestion();
			});
		},
		loadLevel = function() {
			var level_img = new Image;

			level_img.onload = function() {
				$image[0].src = this.src;

				getQuestion();
				initImageScroll();

				// prefetch other level visuals after the initial level visual is loaded
				var other_images = [];

				for(var i = 0; i < total_level; i++) {
					other_images[i] = new Image;
					other_images[i].src = '/images/levels/level_' + i + '.jpg';
				}
			};

			level_img.src = '/images/levels/level_' + level + '.jpg';
		},
		initLevel = function() {
			stage = 0;
			score = 0;

			if(sessionStorage.getItem('level')) {
				var session_level = parseInt(sessionStorage.getItem('level'));

				session_level = session_level >= total_level? 0 : session_level;

				setLevel(session_level);
			}

			updateLevelUI();
			updateScoreUI();
			loadLevel();

			if(level === 2) {
				initTimer();
				lose = false;
			}
		},
		showSection = function(sec) {
			var to_show = sec || 0,
				$to_show = $game_sec[to_show],
				to_hide = [],
				hide_c = 'game-section--hide',
				anim_duration = 300;

			$.each($game_sec, function(k, sec) {
				if(k !== to_show) {
					to_hide.push($(sec));
				}
			});

			$.each(to_hide, function(k, sec) {
				var $hide = $(sec);

				$hide.addClass(hide_c);

				setTimeout(function() {
					$hide.hide().removeClass(hide_c);
				}, anim_duration);
			});

			setTimeout(function() {
				$to_show.show();

				// init level if section to show is play
				if(to_show === 0) {
					initLevel();
				}
			}, anim_duration + 100);
		};



	if($view) {
		showSection(0);

		$popup_close.click(function() {
			afterIntro();
			hidePopup($popup);
		});

		$popup.click(function() {
			afterIntro();
		});

		$help.click(function() {
			if(level === 2 && timer) {
				timer.pause();
			}
			showPopup($popup);
		});

		$game_win.click(function(e) {
			e.preventDefault();
			e.stopPropagation();
		});

		$close_win.click(function(e) {
			hideWin();
		});

		$win_retry.click(function(e) {
			setLevel(0);
			window.location.replace('/receipt');
		});

		$lose_retry.click(function(e) {
			setLevel(2);
			showSection(0);
		});

		$facebook.click(function(e) {
			e.preventDefault();

			FB.ui({
				method: 'share',
				display: 'popup',
				href: fb_link,
				quote: fb_quote,
				mobile_iframe: true,
			}, function(response){});
		});
	}

	$w.resize(function() {
		requestAnimationFrame(setBlockSizePos);
		requestAnimationFrame(hideAnswerList);
	});
}

var is_game = $('#game_container').length? true : false;

$(function() {
	if(is_game) {
		initGame();
	}
});
